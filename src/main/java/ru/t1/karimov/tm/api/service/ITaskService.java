package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name, String description) throws AbstractFieldException;

    Task create(String name) throws AbstractFieldException;

    List<Task> findAllByProjectId(String projectId);

    Task updateById(String id, String name, String description) throws AbstractException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractException;

    Task changeTaskStatusById (String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex (Integer index, Status status) throws AbstractException;

}
