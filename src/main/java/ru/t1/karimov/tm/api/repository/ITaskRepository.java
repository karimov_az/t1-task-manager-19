package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    Task create(String name, String description) throws AbstractFieldException;

    Task create(String name) throws AbstractFieldException;

    List<Task> findAllByProjectId(String projectId);

}
