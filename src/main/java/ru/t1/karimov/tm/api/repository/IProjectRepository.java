package ru.t1.karimov.tm.api.repository;

import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name, String description) throws AbstractFieldException;

    Project create(String name) throws AbstractFieldException;

}
