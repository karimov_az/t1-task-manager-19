package ru.t1.karimov.tm.api.service;

import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

public interface IProjectService extends IService<Project> {

    Project create(String name) throws AbstractFieldException;

    Project create(String name, String description) throws AbstractFieldException;

    Project updateById(String id, String name, String description) throws AbstractException;

    Project updateByIndex(Integer index, String name, String description) throws AbstractException;

    Project changeProjectStatusById (String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex (Integer index, Status status) throws AbstractException;

}
