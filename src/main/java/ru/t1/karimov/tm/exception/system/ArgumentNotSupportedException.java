package ru.t1.karimov.tm.exception.system;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException() {
        super("Error! Argument not supported...");
    }

    public ArgumentNotSupportedException(String argument) {
        super("Error! Argument \"" + argument + "\" not supported...");
    }

}
