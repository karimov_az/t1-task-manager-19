package ru.t1.karimov.tm.repository;

import ru.t1.karimov.tm.api.repository.IProjectRepository;
import ru.t1.karimov.tm.exception.field.AbstractFieldException;
import ru.t1.karimov.tm.model.Project;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public Project create(final String name, final String description) throws AbstractFieldException {
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public Project create(final String name) throws AbstractFieldException {
        final Project project = new Project();
        project.setName(name);
        return add(project);
    }

}
