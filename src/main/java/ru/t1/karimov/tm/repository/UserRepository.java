package ru.t1.karimov.tm.repository;

import ru.t1.karimov.tm.api.repository.IUserRepository;
import ru.t1.karimov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user : records) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : records) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public boolean isLoginExist(final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(final String email) {
        return findByEmail(email) != null;
    }

}
