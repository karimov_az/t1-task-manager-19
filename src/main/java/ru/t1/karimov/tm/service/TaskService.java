package ru.t1.karimov.tm.service;

import ru.t1.karimov.tm.api.repository.ITaskRepository;
import ru.t1.karimov.tm.api.service.ITaskService;
import ru.t1.karimov.tm.enumerated.Status;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.entity.TaskNotFoundException;
import ru.t1.karimov.tm.exception.field.*;
import ru.t1.karimov.tm.model.Task;

import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractService<Task, ITaskRepository> implements ITaskService {

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
    }

    @Override
    public Task create(final String name) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Task create(final String name, final String description) throws AbstractFieldException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return repository.findAllByProjectId(projectId);
    }

    @Override
    public Task updateById(final String id, final String name, final String description) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(
            final Integer index,
            final String name,
            final String description
    ) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize())
            throw new IndexIncorrectException("Error! Index оut of size...");
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Task task = findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) throws AbstractException {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= repository.getSize())
            throw new IndexIncorrectException("Error! Index оut of size...");
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}

