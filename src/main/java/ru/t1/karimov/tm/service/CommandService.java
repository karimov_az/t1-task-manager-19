package ru.t1.karimov.tm.service;

import ru.t1.karimov.tm.api.repository.ICommandRepository;
import ru.t1.karimov.tm.api.service.ICommandService;
import ru.t1.karimov.tm.command.AbstractCommand;
import ru.t1.karimov.tm.exception.AbstractException;
import ru.t1.karimov.tm.exception.system.CommandNotSupportedException;

import java.util.Collection;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(final AbstractCommand command) {
        if (command == null) return;
        commandRepository.add(command);
    }

    @Override
    public AbstractCommand getCommandByArgument(final String argument) {
        if (argument == null || argument.isEmpty()) return null;
        return commandRepository.getCommandByArgument(argument);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) throws AbstractException {
        if (name == null || name.isEmpty()) throw new CommandNotSupportedException();
        return commandRepository.getCommandByName(name);
    }

    @Override
    public Collection<AbstractCommand> getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
